import model.Circle;
import model.Rectangle;
import model.Sharp;
import model.Square;

public class App {
    public static void main(String[] args) throws Exception {
        Sharp sharp1 = new Sharp();
        Sharp sharp2 = new Sharp("Green", false);
        Circle circle1 = new Circle();
        Circle circle2 = new Circle(2.0);
        Circle circle3 = new Circle("Green", true, 3.0);

        Rectangle rectangle1 = new Rectangle();
        Rectangle rectangle2 = new Rectangle(2.5, 1.5);
        Rectangle rectangle3 = new Rectangle("Green", true, 2.0, 1.5);

        Square square1 = new Square();
        Square square2 = new Square(1.5);
        Square square3 = new Square("Green", true, 2.0);

    }
}
