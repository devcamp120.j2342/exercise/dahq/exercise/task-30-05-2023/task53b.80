package model;

public class Square extends Rectangle {
    public Square(double size) {
        super(size, size);
    }

    public Square(String color, boolean fillled, double size) {
        super(color, fillled, size, size);
    }

    public double getSize() {
        return super.getLength();

    }

    public Square() {
    }

    @Override

    public void setWidth(double size) {
        super.setWidth(size);
    }

    @Override
    public void setLength(double size) {
        super.setLength(size);
    }

    @Override
    public String toString() {
        return super.toString();
    }

}
