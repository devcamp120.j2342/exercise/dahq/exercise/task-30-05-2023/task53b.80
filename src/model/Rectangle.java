package model;

public class Rectangle extends Sharp {
    private double width = 1.0;
    private double length = 1.0;

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public Rectangle(double width, double length) {
        this.width = width;
        this.length = length;
    }

    public Rectangle(String color, boolean filled, double width, double length) {
        super(color, filled);
        this.width = width;
        this.length = length;
    }

    public Rectangle() {
    }

    public double getArea() {
        return this.width * this.length;
    }

    public double getPerimetter() {
        return (this.width + this.length) * 2;
    }

    @Override
    public String toString() {
        return "Rectangle[ width: " + this.width + " ,length: " + this.length + super.toString() + "]";
    }
}
