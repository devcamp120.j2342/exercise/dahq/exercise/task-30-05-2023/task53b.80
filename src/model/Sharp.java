package model;

public class Sharp {
    private String color = "red";
    private boolean filled = true;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isFilled() {
        return filled;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    public Sharp() {
    }

    public Sharp(String color, boolean filled) {
        this.color = color;
        this.filled = filled;
    }

    @Override
    public String toString() {
        return "Sharp[color: " + this.color + " ,fllled: " + this.filled;
    }

}
