package model;

public class Circle extends Sharp {
    private double radius = 1.0;

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public Circle(String color, boolean filled, double radius) {
        super(color, filled);
        this.radius = radius;
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public Circle() {
    }

    public double getArea() {
        return Math.PI * Math.pow(radius, 2);
    }

    public double getPerimetter() {
        return Math.PI * 2 * radius;
    }

    @Override
    public String toString() {
        return "Circle[ radius: " + this.radius + super.toString() + "]";
    }
}
